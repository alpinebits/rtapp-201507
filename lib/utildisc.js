/*

 AlpineBits rate plan test application (rtapp-201507)

 (C) 2016 AlpineBits Alliance
 based on previous work (C) 2014-2015 TIS innovation park

 utildisc.js - a set of functions to deal with AlpineBits Discount elements (stored as xm2js - JS objects)

 author: chris@1006.org

 */

'use strict';


exports.get_discount = get_get_discount;


/**
 *  get data about discounts in the given rate plan
 *
 * @param   {Object}    rpel - a RatePlan element
 *
 * @returns {Object}    an object with fields: type_free_nights (boolean), maxage, mincnt, lastqp,
 *                                             type_family (boolean), maxage, mincnt and freecnt
 * @throws  {String}    in case of validation errors
 */
function get_get_discount(rpel) {

    var i, j;
    var ret = {type_free_nights: false, type_family: false};


    var offers = rpel.Offers;
    if (!offers) {
        return ret;
    }
    if (offers.length > 1) {
        throw 'invalid RatePlan: more than one Offers elements';
    }

    var offer = offers[0].Offer;

    if (!offer) {
        return ret;
    }

    // there might be 0, 1 or 2 Offer elements:
    // at most 1 with a discount of type "free nights" and at most 1 with a discount of type "family"

    for (i = 0; i < offer.length; i++) {

        var disc = offer[i].Discount;

        if (!disc) {
            throw 'invalid Offer no Discount element';
        }
        if (disc.length > 1) {
            throw 'invalid Offer: more than one Discount element';
        }

        var perc = disc[0].$.Percent;
        var nreq = disc[0].$.NightsRequired;
        var ndis = disc[0].$.NightsDiscounted;
        var patt = disc[0].$.DiscountPattern;

        if (perc != '100') {
            throw 'invalid Offer: missing or invalid Percent attribute in Discount element';
        }

        if (!nreq && !ndis && !patt) {

            // this looks like a family offer

            var guests = offer[i].Guests;
            if (!guests) {
                throw 'invalid Offer: missing Guests element';
            }
            if (guests.length > 1) {
                throw 'invalid Offer: more than one Guests element';
            }
            var guest = guests[0].Guest;
            if (!guest) {
                throw 'invalid Offer: missing Guest element';
            }
            if (guest.length > 1) {
                throw 'invalid Offer: more than one Guest element';
            }

            var ageqcd = guest[0].$.AgeQualifyingCode;
            var maxage = guest[0].$.MaxAge;
            var mincnt = guest[0].$.MinCount;
            var firstp = guest[0].$.FirstQualifyingPosition;
            var lastqp = guest[0].$.LastQualifyingPosition;

            if (ageqcd != '8') {
                throw 'invalid Offer: invalid value for AgeQualifyingCode';
            }
            if (!maxage || !is_positive_int(maxage)) {
                throw 'invalid Offer: missing or invalid MaxAge attribute';
            }
            if (!mincnt || !is_non_negative_int(mincnt)) {
                throw 'invalid Offer: missing or invalid MinCount attribute';
            }
            if (firstp !== '1') {
                throw 'invalid Offer: invalid value for FirstQualifyingPosition';
            }
            if (!lastqp || !is_positive_int(lastqp)) {
                throw 'invalid Offer: missing or invalid LastQualifyingPosition';
            }
            if (mincnt > lastqp) {
                throw 'invalid Offer: MinCount cannot exceed LastQualifyingPosition';
            }
            if (ret.type_family) {
                throw 'invalid Offer: more than one discounts of type "family" detected';
            }
            ret.type_family = true;
            ret.maxage = maxage;
            ret.mincnt = mincnt;
            ret.freecnt = lastqp;

        } else if (nreq && ndis && patt) {

            // this looks like a free nights offer

            if (!is_positive_int(nreq) || nreq > 999) {
                throw 'invalid Offer: invalid value for NightsRequired';
            }
            if (!is_positive_int(ndis) || ndis > 999) {
                throw 'invalid Offer: invalid value for NightsDiscounted';
            }
            var testpatt = '';
            for (j = 0; j < nreq - ndis; j++) {
                testpatt += '0';
            }
            for (j = 0; j < ndis; j++) {
                testpatt += '1';
            }
            if (testpatt !== patt) {
                throw 'invalid Offer: inconsistent values for NightsRequired, NightsDiscounted and DiscountPattern';
            }
            if (ndis > nreq) {
                throw 'invalid Offer: NightsDiscounted cannot exceed NightsRequired';
            }
            if (ret.type_free_nights) {
                throw 'invalid Offer: more than one discounts of type "free nights" detected';
            }
            ret.type_free_nights = true;
            ret.nreq = nreq;
            ret.ndis = ndis;

        } else {
            throw 'invalid Offer: type of discount cannot be determined from the attributes of the Discount element';
        }

    }

    return ret;

}


/* private functions */


function is_non_negative_int(a) {

    var s = String(a);
    var p = s.match(/^\d+$/);
    if (!p) {
        return false;
    }
    return true;
}

function is_positive_int(a) {

    if (is_non_negative_int(a) && a > 0) {
        return true;
    }
    return false;
}